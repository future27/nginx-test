import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

(async function bootstrap() {
  const PORT = 5001 || 6001;
  const app = await NestFactory.create(AppModule);
  await app.listen(PORT, () =>
    console.log(`Application is started on port: ${PORT}`),
  );
})();
