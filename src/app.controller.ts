import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('timeAPI')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('instant')
  @HttpCode(HttpStatus.OK)
  getInstant(): string {
    return this.appService.getInstant();
  }

  @Get('delayed')
  async getDelayed(): Promise<string> {
    return await this.appService.getDelayed();
  }
}
