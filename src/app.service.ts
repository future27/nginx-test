import { Injectable } from '@nestjs/common';
import * as uuid from 'uuid';

@Injectable()
export class AppService {
  getInstant(): string {
    return `<${uuid.v4()}> success moment!`;
  }

  async getDelayed(): Promise<string> {
    return new Promise((res) => {
      setTimeout(() => {
        res(`<${uuid.v4()}> success after 7sec!`);
      }, 7000);
    });
  }
}
